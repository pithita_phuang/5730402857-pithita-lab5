package pithia.example1.domparser;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class DOMParser_Traversal {
public static void main(String[] args) {
        String filename = "nation.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document docNode = parser.parse(filename);
           
            Element rootElem = docNode.getDocumentElement();
            followNode(rootElem);
            
        } catch (Exception ex) {
            System.err.println("some problem");
        }

    }

public static void followNode(Node node) throws IOException {
    writeNode(node);
        if (node.hasChildNodes()) {
            String name = node.getNodeName();
            int numChildren = node.getChildNodes().getLength();
            System.out.println("node " + name + " has " + numChildren + " children ");
            Node firstChild = node.getFirstChild();
            followNode(firstChild);
        }
        Node nextNode = node.getNextSibling();
            if(nextNode != null)followNode(nextNode);
    }

    private static void writeNode(Node node) {
       System.out.println("Node:type = " + node.getNodeType() + " name = " + node.getNodeName() + " value = " + node.getNodeValue());//To change body of generated methods, choose Tools | Templates.
    }

    
        
}

    

