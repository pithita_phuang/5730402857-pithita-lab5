package pithia.example1.domparser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class DOMPaser {

    public static void main(String[] args) {
        String filename = "nation.xml";

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder parser = factory.newDocumentBuilder();
            Document docNode = parser.parse(filename);
            Element rootElem = docNode.getDocumentElement();
            
            System.out.print("the root element name is " + rootElem.getNodeName() + "\n");
            
        } catch (Exception ex) {
            System.err.println("some problem");
        }

    }
}
